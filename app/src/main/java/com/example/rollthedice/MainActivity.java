package com.example.rollthedice;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import java.util.Random;

public class MainActivity extends AppCompatActivity {
    private ImageView diceImg;
    private Random random = new Random();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        diceImg = findViewById(R.id.diceImg);
        diceImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rollTheDice();
            }
        });
    }

    private void rollTheDice(){
        int rand = random.nextInt(6) + 1;
        switch(rand){
            case 1:
                diceImg.setImageResource(R.drawable.dice1);
                break;

            case 2:
                diceImg.setImageResource(R.drawable.dice2);
                break;

            case 3:
                diceImg.setImageResource(R.drawable.dice3);
                break;

            case 4:
                diceImg.setImageResource(R.drawable.dice4);
                break;

            case 5:
                diceImg.setImageResource(R.drawable.dice5);
                break;

            case 6:
                diceImg.setImageResource(R.drawable.dice6);
                break;
        }
    }

}
